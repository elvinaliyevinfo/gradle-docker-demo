FROM openjdk:17
COPY build/libs/gradle-docker-demo-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
EXPOSE 8080
ENTRYPOINT ["java"]
CMD ["-jar", "/app/gradle-docker-demo-0.0.1-SNAPSHOT.jar"]
